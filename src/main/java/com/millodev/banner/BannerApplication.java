package com.millodev.banner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BannerApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(BannerApplication.class);
		/**
		 * turn off the banner display enabling the line below. 
		 */
		// app.setBannerMode(Mode.OFF);
		app.run(args);
	}

}